import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

const image1 = require('./assets/one.jpg');
const image2 = require('./assets/two.jpg');
const image3 = require('./assets/three.jpg');

class App extends React.Component {
  state = {
    showDetail: false,
    list: [
      {
        image: image1,
        name: 'Hamburger',
        price: '12.99$',
        description: 'Hamburger is delicious',
        id: 1,
      },

      {
        image: image2,
        name: 'Pizza',
        price: '15.99$',
        description: 'I like eating Pizza because it is delecious',
        id: 2,
      },

      {
        image: image3,
        name: 'Potato Chip',
        price: '5.77$',
        description: 'This is ready-made food',
        id: 3,
      },
    ],
  };

  showItemDetail() {
    return (
      <ScrollView>
        <View style={{padding: 10, flex: 1, backgroundColor: '#f0f0f0'}}>
          {this.state.list.map((item) => {
            return (
              <View style={styles.itemDetailContainer}>
                <Image source={item.image} style={styles.imageStyle} />
                <Text style={{fontWeight: 'bold', marginBottom: 10}}>
                  {item.name}
                </Text>
                <Text>{item.price}</Text>
                <Text>{item.description}</Text>
              </View>
            );
          })}
        </View>
      </ScrollView>
    );
  }

  render() {
    if (this.state.showDetail) {
      return this.showItemDetail();
    }
    return (
      <View style={styles.container}>
        {this.state.list.map((item) => (
          <TouchableOpacity
            key={item.id}
            activeOpacity={0.6}
            onPress={() => {
              this.setState({showDetail: true});
            }}>
            <View style={styles.list}>
              <Image source={item.image} style={styles.imageItem} />
              <Text>{item.name}</Text>
              <Text>{item.price}</Text>
            </View>
          </TouchableOpacity>
        ))}
        <Text>{this.state.showDetail}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    margin: 10,
  },

  list: {
    backgroundColor: 'lightgrey',
    marginVertical: 5,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    height: 70,
  },

  imageItem: {
    width: 50,
    height: 50,
  },

  itemDetailContainer: {
    alignItems: 'center',
    marginVertical: 15,
    backgroundColor: '#fff',
    padding: 10,
  },

  imageStyle: {
    width: 200,
    height: 200,
  },
});

export default App;
